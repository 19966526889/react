// rcc
// 双向数据绑定
import React, { Component } from "react";

export default class App extends Component {
  // 输入框的数据会变化, 用 state 保存
  state = { data: "123456" };

  render() {
    return (
      <div>
        {/* 
        vue中 v-model 是语法糖写法, 自动完成了两个操作:
        - 把值填充到 输入框
        - 输入框变化时, 把变化更新给值
        */}
        {/* onChange: 输入框有变化时触发 */}
        <input type="text" value={this.state.data} onChange={this._changed} />
        <div>{this.state.data}</div>
        {/* 由于箭头函数简单, 可以直接写到JSX中 */}
        {/* 语法糖: ()=> {return xxxx;}  可以简写: ()=> xxxx */}
        <input
          type="text"
          value={this.state.data}
          onChange={(e) => this.setState({ data: e.target.value })}
        />
      </div>
    );
  }
  // _ 习惯把 与事件绑定的函数, 添加 _ 前缀, 不强制
  _changed = (e) => {
    console.log(e.target.value);
    // 把新的值 更新给 data
    this.setState({ data: e.target.value });
  };
}
