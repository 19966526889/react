// rcc
// map函数
import React, { Component } from "react";

export default class App extends Component {
  skills = ["vue", "angular", "react", "jQuery"];

  showBtns1() {
    //数组.map(func): 遍历数组中的每个元素, 用func进行处理, 最终把func的处理结果 组成新的数组, 作为 map 的返回值
    const arr = this.skills.map((item, index) => {
      return <button key={index}>{item}</button>;
    });

    return arr;
  }

  showBtns2() {
    return this.skills.map((item, index) => {
      return <button key={index}>{item}</button>;
    });
  }

  // 箭头函数语法糖:  ()=> {return xxx;}   简化  ()=> xxx
  showBtns3() {
    return this.skills.map((item, index) => (
      <button key={index}>{item}</button>
    ));
  }

  // 最终格式: 利用各种语法糖简化代码
  showBtns = () =>
    this.skills.map((item, index) => <button key={index}>{item}</button>);

  render() {
    return <div>{this.showBtns()}</div>;
  }
}
