// rcc
import React, { Component } from "react";
/**
 * 在html中引入css: <link ...
 * 在css中引入css:  @import
 * 在 js 中引入 css: import
 */
// 歧义: import 本身是用于引入模块, 当用于引入文件时, 必须添加路径标识--  ./ ../  /
import "./App.css";

export default class App extends Component {
  render() {
    return (
      <div>
        <div className="danger">DANGER</div>
      </div>
    );
  }
}
