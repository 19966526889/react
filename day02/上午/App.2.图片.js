// rcc
// 本地图片: 必须手动创建 src/assets 目录, 图片资源都存放在此目录中
import React, { Component } from "react";

//另一种引入方式:
import png_113 from "./assets/113.png";

export default class App extends Component {
  render() {
    return (
      <div>
        <img src={png_113} width="50" alt="" />
        {/* webpack: 工具要求, 必须使用 require关键词引入的图片, 才会被打包! */}
        {/* JS中使用图片, 必须用require来标识, 才会被webpack识别及打包,  vue也是这个机制 */}
        {/* default: 代表引入内容中的默认值: 图片 */}
        <img width="200" src={require("./assets/113.png").default} alt="" />
      </div>
    );
  }
}
