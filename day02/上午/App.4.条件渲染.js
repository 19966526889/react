// rcc
// 条件渲染: 满足指定条件后 显示对应的元素
import React, { Component } from "react";

export default class App extends Component {
  state = { score: 60 };

  // delta: 变化量, 取值 +10 或 -10
  changeScore(delta) {
    // setState: 更新数据 + 更新界面(再次调用render方法)
    this.setState({ score: this.state.score + delta });
  }

  // react没有语法糖的if写法, 只能用原生的if判断
  showRes() {
    const score = this.state.score;

    if (score < 60) return <div>不及格</div>;
    if (score >= 60 && score < 80) return <div>良好</div>;
    if (score >= 80 && score < 100) return <div>优秀</div>;

    return <div style={{ backgroundColor: "orange", padding: 10 }}>完美</div>;
  }

  render() {
    console.log("render方法执行了");

    return (
      <div>
        <h3>考试分数: {this.state.score}</h3>
        <button
          disabled={this.state.score === 0}
          onClick={this.changeScore.bind(this, -10)}
        >
          减10分
        </button>
        <button
          disabled={this.state.score === 100}
          onClick={() => this.changeScore(10)}
        >
          加10分
        </button>

        {this.showRes()}
      </div>
    );
  }
}
