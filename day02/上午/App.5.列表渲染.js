// rcc
// 列表渲染: for
import React, { Component } from "react";

export default class App extends Component {
  skills = ["html", "css", "sass", "bootstrap", "react", "vue", "angular"];

  emps = [
    { name: "东东", age: 34, gender: 1 },
    { name: "亮亮", age: 32, gender: 1 },
    { name: "亚楠", age: 24, gender: 0 },
    { name: "梦瑶", age: 18, gender: 0 },
  ];

  // 期望: 把数组的每个元素放在 按钮中显示: button
  showBtns() {
    const arr = [];
    // 遍历数组, 把每一项组成一个按钮, 添加到数组里
    this.skills.forEach((item, index) => {
      // JSX存放在数组中, 则每一项要唯一标识key
      arr.push(<button key={index}>{item}</button>);
    });

    return arr;
  }

  showLis() {
    const arr = [];

    this.skills.forEach((item, index) => {
      arr.push(<li key={index}>{item}</li>);
    });

    return arr;
  }

  showTrs() {
    const arr = [];

    this.emps.forEach((item, index) => {
      arr.push(
        <tr key={index}>
          <td>{index + 1}</td>
          <td>{item.name}</td>
          <td>{item.age}</td>
          {/* react 没有 过滤器写法, 直接写JS处理 */}
          <td>{item.gender == 0 ? "女" : "男"}</td>
        </tr>
      );
    });

    return arr;
  }

  render() {
    return (
      <div>
        {/* 数组的处理方式: 自动把每个元素显示在页面 */}
        <div>{this.skills}</div>
        <div>{this.showBtns()}</div>
        <ol>{this.showLis()}</ol>

        <table border="1">
          <thead>
            <tr>
              <td>序号</td>
              <td>姓名</td>
              <td>年龄</td>
              <td>性别</td>
            </tr>
          </thead>
          <tbody>{this.showTrs()}</tbody>
        </table>
      </div>
    );
  }
}
