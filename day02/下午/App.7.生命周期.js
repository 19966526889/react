// 生命周期
// rcc
import React, { Component } from "react";

export default class App extends Component {
  state = { show: false, count: 100 };

  render() {
    return (
      <div>
        <button onClick={() => this.setState({ show: !this.state.show })}>
          添加/移除
        </button>
        <button onClick={() => this.setState({ count: this.state.count + 1 })}>
          count: {this.state.count}
        </button>
        {this.state.show ? <Son count={this.state.count} /> : null}
      </div>
    );
  }
}

//子组件
class Son extends Component {
  state = { num: 1 };

  componentDidMount() {
    console.log("componentDidMount: 组件挂载完毕");
  }

  // 面试题: 如何提高React的运行效率?
  // 根据实际的业务需求, 减少页面的渲染次数.  例如下方的例子, 数字<=20之前都不用刷新界面.
  shouldComponentUpdate(props, state) {
    // 当数据发生变更时, setState 会重新触发 render方法来重绘界面.
    // 当前函数的返回值, 可以决定是否要 重新触发 render 方法
    // true: 重绘
    // false: 不重绘
    console.log("shouldComponentUpdate");
    console.log("props的新值:", props);
    console.log("state的新值:", state);

    // 如果 state.num > 20 的时候, 就恢复正常刷新
    if (state.num > 20) return true;

    return false;
  }

  /**
   * 组件中有两个特殊的属性:
   * props: 接收外部传参  <Son name="ddd"/>
   * state: 组件内部的属性, 会发生变更, 配合 setState 使用
   */
  componentDidUpdate(props, state) {
    console.log("componentDidUpdate 组件发生更新");
    console.log("更新前state:", state);
    console.log("更新后state:", this.state);

    console.log("更新前props:", props);
    console.log("更新后props:", this.props);
  }

  componentWillUnmount() {
    console.log("componentWillUnmount: 组件将要卸载");
  }

  render() {
    console.log("render 被触发!");

    return (
      <div>
        <h3>我是子组件</h3>
        <button onClick={() => this.setState({ num: this.state.num + 1 })}>
          num:{this.state.num}
        </button>
      </div>
    );
  }
}
