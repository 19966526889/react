// rcc
// 网络操作

import React, { Component } from "react";

export default class App extends Component {
  // 数据要显示在页面上, 网络请求获取的, 初始值是null
  state = { res: null };

  componentDidMount() {
    // 挂载时, 发送请求
    // axios:  axios.get(url).then(res=>{}).catch(err=>{})
    // react 提供了请求的模块: fetch
    const url = "http://101.96.128.94:9999/mfresh/data/news_select.php";

    fetch(url).then((res) => {
      // res: 是请求的所有返回内容, 需要用 json进行解析
      // console.log(res.json());
      res.json().then((res) => {
        console.log(res);
      });
    });

    // 使用链式写法, 简化上方写法
    fetch(url)
      .then((res) => res.json())
      .then((res) => {
        console.log(res);
        this.setState({ res }); // {res:res}
      });
  }

  showData() {
    return this.state.res.data.map((item, index) => {
      return (
        <div key={index}>
          <span>{item.title}</span>
          <span>{item.pubTime}</span>
        </div>
      );
    });
  }

  render() {
    // 网络请求获取的数据 在使用之前应该判断存在才能用
    if (this.state.res === null) return <div></div>;

    const { pageNum, pageCount } = this.state.res;

    return (
      <div>
        <div>总页数: {pageCount}</div>
        <div>当前页: {pageNum}</div>
        <div>{this.showData()}</div>
      </div>
    );
  }
}
