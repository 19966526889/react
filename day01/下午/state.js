// setState 的小demo

// 假设 setState 刷新界面耗时 1s

let props = { num: 5 };

function setState(args, callback) {
  //假设需要 1s 才能更新DOM结束
  setTimeout(() => {
    props = args;
    // console.log("更新结束:", props);
    callback();
  }, 1000);
}

setState({ num: 6 }, () => {
  console.log("更新完毕:", props);
});

console.log("props:", props);
