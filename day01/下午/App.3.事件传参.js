// rcc
// 带有参数的事件
import React, { Component } from "react";

export default class App extends Component {
  show(name) {
    console.log(name);
  }

  render() {
    // {}中的代码会立即执行, 事件传参时, 就需要提前绑定参数, 但是不执行
    // bind: 把参数绑定给函数, 返回一个绑定了参数的 新函数
    //    -- 参数1 必须是 this 的指向,  剩余参数为 其他值
    const show = this.show.bind(this, "东东");

    return (
      <div>
        <button onClick={show}>东东</button>
        <button onClick={this.show.bind(this, "亮亮")}>亮亮</button>
        {/* 利用箭头函数, 点击后执行箭头函数, 箭头函数再执行内部的 */}
        <button onClick={() => this.show("铭铭")}>铭铭</button>
      </div>
    );
  }
}
