// setState 的异步性
import React, { Component } from "react";

export default class App extends Component {
  state = { count: 5 };

  doAdd() {
    // setState: 需要刷新界面, 假设刷新需要 1秒
    // 如果等待 1s 刷新后, 再执行后续代码, 会导致程序卡顿
    // 做法: 把刷新操作放在多线程中执行, 不会影响主线程的代码执行
    // setState的参数2 是渲染完毕后的回调函数
    this.setState({ count: this.state.count + 1 }, () => {
      console.log("DOM刷新完毕:", this.state);
    });

    console.log("count:", this.state.count);
  }

  render() {
    return (
      <div>
        <button onClick={() => this.doAdd()}>{this.state.count}</button>
      </div>
    );
  }
}
