// rcc
// 样式的绑定
import React, { Component } from "react";

export default class App extends Component {
  state = { size: 20 };

  doBig() {
    this.setState({ size: this.state.size + 2 });
  }

  render() {
    return (
      <div>
        {/* 在DOM中, style是 对象类型 */}
        {/* 外层{} 代表内容为 JS代码 */}
        {/* 内层{} 是对象的包围 */}
        {/* 字体大小, 默认单位是 px , 可以不写 */}
        <div style={{ color: "red", fontSize: this.state.size }}>
          Hello World!
        </div>
        <button onClick={this.doBig.bind(this)}>变大</button>
      </div>
    );
  }
}
