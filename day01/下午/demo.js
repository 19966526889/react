// 普通函数: 谁调用,则内部的this 指向谁
// 箭头函数: 函数中this永远指向声明时所在位置

class Demo {
  name = "Demo";

  // 普通函数: 谁调用,则内部的this 指向谁
  show1() {
    console.log("普通函数:", this.name);
  }

  // 箭头函数: 函数中this永远指向声明时所在位置
  show2 = () => {
    console.log("箭头函数:", this.name);
  };
}

let d = new Demo();
d.show1();
d.show2();

console.log("------------------------------");

let a = { name: "aaa", show1: d.show1, show2: d.show2 };
a.show1();
a.show2();

// 针对普通函数 this 的指向问题, 官方提供了 3个 相关函数: apply call bind
// apply, call:  在替换this的同时, 触发函数
// bind : 返回替换了this之后的 新函数

let c = { name: "ccc" };
a.show1.call(c); // 把 show1 中的this 替换成 c

// bind: 返回绑定c作为this的新的函数
let show11 = a.show1.bind(c);
show11(); //手动触发 绑定了c 的新函数
