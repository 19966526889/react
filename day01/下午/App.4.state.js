// 状态值
// state 与 setState
/**
 * 在微信小程序中, 有  data 和 setData,
 * data中的数据可以显示在DOM中, 利用setData更新数据, DOM才会刷新
 *
 * 此做法实际是借鉴了 React 的 state 机制
 */
import React, { Component } from "react";

export default class App extends Component {
  // 如果属性要绑定到 DOM 并且 要刷新变化, 则存储在 state 中
  state = { num: 1 };

  doAdd() {
    // 利用 setState 才能在更新数据的同时 刷新界面
    this.setState({ num: this.state.num + 1 });

    // this.num++;
    // console.log(this.num);
  }

  render() {
    return (
      <div>
        <button onClick={this.doAdd.bind(this)}>{this.state.num}</button>
      </div>
    );
  }
}
