// rcc
// 事件中的 this 问题
import React, { Component } from "react";

export default class App extends Component {
  name = "东东";

  show() {
    // 点击事件, 在严格模式下, 执行的函数, 其this指向undefined
    console.log(this.name);
  }

  //箭头函数: 自带this保持, 与调用者无关
  show1 = () => {
    // console.log(this.name);
    alert(this.name);
  };

  // 组件使用时: new App().render();
  // 普通函数: 谁调用,则内部的this 指向谁
  // 箭头函数: 函数中this永远指向声明时所在位置
  render() {
    console.log("render 的this:", this);
    // 利用bind 为事件函数指定this
    // show: 一个替换了this的新函数
    const show = this.show.bind(this);

    return (
      <div>
        <h3>{this.name}</h3>
        <button onClick={show}>点击事件</button>
        <button onClick={this.show.bind(this)}>点击事件</button>
        <button onClick={this.show1}>箭头函数</button>
        {/* 利用箭头函数的this保持特征, 让普通函数可用 */}
        {/* 点击后执行的是 箭头函数, 箭头函数再执行内部的 this.show() */}
        <button onClick={() => this.show()}>箭头+普通</button>
        {/* {}中的代码会直接执行, 所以事件绑定的函数, 不要写(), 会导致直接执行 */}
        <button onClick={this.show1}>{9 + 9}</button>
      </div>
    );
  }
}
