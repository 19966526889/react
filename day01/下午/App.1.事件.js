// 前提: 安装过 React 插件--详见文档
// rcc : 插件提供的快捷代码块
import React, { Component } from "react";

// 关于代码自动美化: 每种类型的文件 都需要配置一次
// 右键代码 -> 使用...格式化 -> 配置默认 -> Prettier
export default class App extends Component {
  show() {
    console.log(new Date().toLocaleTimeString());
  }

  render() {
    return (
      <div>
        <h1>Hello World!</h1>
        {/*  
        点击事件: 
        - 原生: onclick
        - vue : @click
        之前书写的时候都是 "" 包围,  @click="show" 因为这些代码都是在 HTML 中书写的
        */}
        {/* {} 代表内部是 JS 代码 */}
        {/* this: 关键词, 代表当前对象 */}
        <button onClick={this.show}>点击事件</button>
      </div>
    );
  }
}
